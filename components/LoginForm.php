<?php

require_once webroot.'/core/Form.php';
require_once 'User.php';

/**
 * Login form
 */
class LoginForm extends Form
{

    public $login;
    public $password;

    /**
     * Validate form
     * @return boolean
     */
    public function validate()
    {
        if ($this->validateRequired())
        {
            if (!$this->authenticate($this->login, $this->password))
            {
                $this->errors[] = 'Пользователь c таким логином и паролем не найден';
            }
            else
                return true;
        }

        return false;
    }

    public function authenticate($login, $password)
    {
        $user = User::findUserByLogin($login);

        if (is_null($user) || $password !== $user->password)
            return false;

        return true;
    }

    /**
     * Required validator
     * @return boolean
     */
    protected function validateRequired()
    {
        foreach ($this->attributes as $attribute => $value)
        {
            $this->{$attribute} = trim($this->{$attribute});

            if (strlen($this->{$attribute}) == 0)
            {
                $this->errors[] = 'Заполните логин и пароль';
                return false;
            }
        }

        return true;
    }

}
