<?php

/*
 * Represent user
 */
class User
{

    public $login;
    public $password;

    public function __construct($login, $password)
    {
        $this->login = $login;
        $this->password = $password;
    }

    /**
     * Defined users for test without db
     * @return array of pairs login => password
     */
    private static function getDefinedUsers()
    {
        return array(
            array('test' => 'test'),
            array('admin' => 'admin')
        );
    }

    /**
     * Find single user by login
     * @param string $login
     */
    public static function findUserByLogin($login)
    {
        $users = self::getDefinedUsers();

        foreach ($users as $row)
        {
            $defLogin = key($row);
            $defPassword = current($row);

            if ($login === $defLogin)
            {
                return new User($defLogin, $defPassword);
            }
        }

        return null;
    }

}
