<?php
require_once 'LoginForm.php';
/*
 * Pseudo controller class
 */
class Controller
{

    public function __construct()
    {
    }

    public function run()
    {
        $this->actionIndex();
    }

    public function actionIndex()
    {
        $model = new LoginForm();

        if (isset($_POST['LoginForm']))
        {
            $model->setAttributes($_POST['LoginForm']);

            if ($model->validate())
            {
                $user = User::findUserByLogin($model->login);
                $sid  = App::getInstance()->session->login($user);

                header('Location: index.php?'.$sid);
            }
        }

        $this->render('login', array('model'=>$model));
    }

    /**
     * Append session id to link
     * @param string $url
     */
    public function createUrl($url)
    {
        if (App::getInstance()->session->isAuthenticated())
        {
            if (strpos($url, '&'))
            {
                $url .= '&';
            }

            $url .= App::getInstance()->session->getSid();
        }

        return $url;
    }

    public function render($viewName, $params = array())
    {
        extract($params);
        require_once "views/{$viewName}.php";
    }
}
