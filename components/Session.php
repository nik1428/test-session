<?php

require_once 'User.php';
/*
 * Session class
 */
class Session
{

    const SESSION_NAME = 'SID';
    const SECRET = 'wJBjLqU4zo';

    public $sessionId;
    public $username;

    private $_authenticated = false;

    public function isAuthenticated()
    {
        return $this->_authenticated;
    }

    public function __construct()
    {
        $this->session_start();
    }


    /**
     * Check user session valid or not
     * @return boolean
     */
    public function session_start()
    {

        if (!empty($_GET[self::SESSION_NAME]))
        {
            $data = explode('_', $_GET[self::SESSION_NAME]);

            if (count($data) < 2)
            {
                throw new Exception('Session invalid');
            }
            else
            {
                $login = $data[0];

                $sessionValid = $this->validateSession($login, $_GET[self::SESSION_NAME]);

                if ($sessionValid)
                {
                    $this->_authenticated = true;
                    $this->username  = $login;
                    $this->sessionId = $_GET[self::SESSION_NAME];
                }
            }
        }

        return (bool) $this->sessionId;
    }

    /**
     * Validate session id
     * @param string $login User login
     * @param string $sessionId
     */
    protected function validateSession($login, $sessionId)
    {
        $user = User::findUserByLogin($login);

        if ($user)
        {
            $actualSessionId = $this->generateSessionId($user);

            if ($sessionId == $actualSessionId)
                return true;
        }

        return false;
    }

    protected function generateSessionId(User $user)
    {
        $this->sessionId = $user->login . '_' . md5(self::SECRET . $user->login . $user->password);
        return $this->sessionId;
    }


    public function getSid()
    {
        return self::SESSION_NAME . '=' . $this->sessionId;
    }

    /**
     * @return SID
     */
    public function login(User $user)
    {
        $this->generateSessionId($user);
        return $this->getSid();
    }
}
