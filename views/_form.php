<h1>Форма входа</h1>

<div style="width: 400px;">
    <?php if ($model->getErrors()): ?>
        <div style="color:red; padding:10px; border: 1px solid red;">
            <?php
            foreach ($model->getErrors() as $error)
                echo $error;
            ?>
        </div>
    <?php endif; ?>

    <form method="POST" action="">
        <div>Логин: <?php echo $model->renderInputField('login', 'text'); ?></div>
        <div>Пароль: <?php echo $model->renderInputField('password', 'password'); ?></div>
        <input type="submit" value="Авторизоваться"/>
    </form>
    Пользователи для теста: 
    Логин: test Пароль: test
</div>