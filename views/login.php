<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
    </head>
    <body>
        <?php if (!App::getInstance()->session->isAuthenticated()): ?>
             <?php $this->render('_form', array('model'=>$model));?>
        <?php else:?>
            <h1>Добро пожаловать, <i><?php echo App::getInstance()->session->username;?></i></h1>
        <?php endif;?>
    </body>
</html>
