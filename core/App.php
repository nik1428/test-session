<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once webroot.'/components/Session.php';

class App
{
    /** @var Session */
    public $session;
    public $defaultController = 'Controller';

    private static $_instance;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * @return App
     */
    static function getInstance()
    {
        if (!self::$_instance)
            self::$_instance = new self;

        return self::$_instance;
    }

    public function run()
    {
        require_once webroot.'/components/'.$this->defaultController.'.php';
        $controller  = new Controller();
        $controller->run();
    }

}
