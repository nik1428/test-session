<?php

/*
 * Common component
 */

class Form
{

    /** validation errors
     * @var array
     * */
    protected $errors;

    /**
     * @var array
     */
    protected $attributes;

    /** Massive attribute assignment */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;

        foreach ($this->attributes as $attribute => $value)
        {
            if (!property_exists($this, $attribute))
                throw new Exception('Unknown LoginForm attrubute ' . $attribute);
            else
            {
                $this->{$attribute} = $value;
            }
        }
    }

    /** @see attributes */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /** @see errors */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Render input field
     * @param string $attribute Model attribute
     * @param string $type Input type
     */
    public function renderInputField($attribute, $type = 'text')
    {
        $fullName = get_class($this) . '[' . $attribute . ']';
        echo "<input type='{$type}' name='{$fullName}' value='{$this->$attribute}'/>";
    }

}